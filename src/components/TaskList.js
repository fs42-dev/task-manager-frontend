import React, { useState } from "react";
import axios from "axios";
import "../TaskList.css";

/**
 * TaskList component displays a list of tasks and allows sorting, marking as completed, and deleting tasks.
 *
 * @param {Array<Object>} tasks - An array of task objects containing information like title, description, dueDate, and completed status.
 * @param {Function} fetchTasks - A function used to refresh the task list from the server.
 * @returns {JSX.Element} - The JSX element representing the task list component.
 */
const TaskList = ({ tasks, fetchTasks }) => {
    const [sortOrder, setSortOrder] = useState("asc"); // Initial sort order (ascending)

    /**
     * Sorts the provided tasks based on the due date in ascending or descending order.
     *
     * @param {Array<Object>} tasks - An array of task objects to be sorted.
     * @returns {Array<Object>} - The sorted array of tasks.
     */
    const sortTasks = (tasks) => {
        return tasks.sort((a, b) => {
            const dateA = new Date(a.dueDate);
            const dateB = new Date(b.dueDate);
            return sortOrder === "asc" ? dateA - dateB : dateB - dateA;
        });
    };

    /**
     * Handles the click event on the sort button, toggling the sort order between ascending and descending.
     */
    const handleSortOrderChange = () => {
        setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    };

    /**
     * Marks a task identified by its ID as completed using an asynchronous PUT request to the server API.
     * Refreshes the task list after a successful update.
     *
     * @param {Number} id - The unique identifier of the task to be marked as completed.
     */
    const markAsCompleted = async (id) => {
        try {
            await axios.put(`http://localhost:8080/api/tasks/${id}/complete`);
            fetchTasks(); // Refresh task list after successful update
        } catch (error) {
            console.error("Erreur lors de la complétion de la tâche", error);
        }
    };

    /**
     * Deletes a task identified by its ID using an asynchronous DELETE request to the server API.
     * Refreshes the task list after a successful deletion.
     *
     * @param {Number} id - The unique identifier of the task to be deleted.
     */
    const deleteTask = async (id) => {
        try {
            await axios.delete(`http://localhost:8080/api/tasks/${id}`);
            fetchTasks(); // Refresh task list after successful deletion
        } catch (error) {
            console.error("Erreur lors de la suppression de la tâche", error);
        }
    };

    return (
        <div>
            <h2>Liste des Tâches (Task List)</h2> {/* Title for the task list */}
            <button onClick={handleSortOrderChange}>
                Trier par date d'échéance ({sortOrder === "asc" ? "Ascendant" : "Descendant"}) {/* Sort button with current sort order */}
            </button>
            <ul>
                {sortTasks(tasks).map((task) => (
                    <li key={task.id} className={`task-item ${task.completed ? "completed" : ""}`}>
                        {" "}
                        {/* Task list item with conditional class for completed tasks */}
                        <input type="checkbox" checked={task.completed} onChange={() => markAsCompleted(task.id)} />{" "}
                        {/* Checkbox to mark task as completed */}
                        <div className="task-info">
                            {" "}
                            {/* Container for task details */}
                            <span className="task-title">{task.title}</span> {/* Task title */}
                            <span className="task-description">{task.description}</span> {/* Task description */}
                            <span className="task-dueDate">{task.dueDate}</span> {/* Task due date */}
                        </div>
                        <button className="delete-button" onClick={() => deleteTask(task.id)}>
                            ❌
                        </button>{" "}
                        {/* Delete button for the task */}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default TaskList;
