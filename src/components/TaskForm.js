import React, { useState } from "react";
import axios from "axios";
import "../TaskForm.css";

/**
 * TaskForm component allows users to create a new task by providing title, description, and due date.
 * It validates user input and sends data to the server API for creation.
 *
 * @param {Function} fetchTasks - A function used to refresh the task list after a successful task creation.
 * @returns {JSX.Element} - The JSX element representing the task creation form.
 */
const TaskForm = ({ fetchTasks }) => {
    const [title, setTitle] = useState(""); // Stores the title of the new task
    const [description, setDescription] = useState(""); // Stores the description of the new task
    const [dueDate, setDueDate] = useState(""); // Stores the due date of the new task
    const [error, setError] = useState(""); // Stores any error message during form validation

    /**
     * Handles the form submission event, validating user input and sending data to the server.
     *
     * @param {Object} e - The event object representing the form submission.
     */
    const handleSubmit = async (e) => {
        e.preventDefault(); // Prevents default form submission behavior

        // Validate title length
        if (title.length < 3 || title.length > 20) {
            setError("Le titre doit comporter entre 3 et 20 caractères.");
            return;
        }

        // Validate description length
        if (description.length < 5 || description.length > 100) {
            setError("La description doit comporter entre 5 et 100 caractères.");
            return;
        }

        // Validate due date
        const today = new Date().toISOString().split("T")[0];
        if (dueDate < today) {
            setError("La date d'échéance ne peut pas être antérieure à aujourd'hui.");
            return;
        }

        try {
            // Send POST request to create the task on the server
            await axios.post("http://localhost:8080/api/tasks", { title, description, dueDate });
            fetchTasks(); // Refresh task list after successful creation
            setTitle(""); // Reset form fields after successful submission
            setDescription("");
            setDueDate("");
            setError("");
        } catch (error) {
            console.error("Erreur lors de l'ajout de la tâche", error);
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>Ajouter une nouvelle tâche (Add a new task)</h2> {/* Form title */}
            <input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                placeholder="Titre (Title)"
                minLength="3"
                maxLength="20"
                required
            />{" "}
            {/* Input for task title with validation attributes */}
            <textarea
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                placeholder="Description"
                minLength="5"
                maxLength="100"
                required
            />{" "}
            {/* Textarea for task description with validation attributes */}
            <input type="date" value={dueDate} onChange={(e) => setDueDate(e.target.value)} required />{" "}
            {/* Input for task due date with validation attribute */}
            {error && <p style={{ color: "red" }}>{error}</p>} {/* Display any validation errors */}
            <button type="submit">Ajouter (Add)</button> {/* Submit button */}
        </form>
    );
};

export default TaskForm;
